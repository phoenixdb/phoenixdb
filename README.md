#CI status

[![CI status](http://ci.phoenixdb.org/buildStatus/icon?job=build_phdb)](http://ci.phoenixdb.org/job/build_phdb/)

# Build instructions

1. Checkout sources:

    git clone --recursive git@bitbucket.org:phoenixdb/phoenixdb.git

2. Go to sources directory:

    cd phoenixdb
    
3. If you already have sources you probably want to checkout or update submodules:

    git submodule init

    git submodule update
    
4. Create some directory for out-of-source build:

    mkdir build && cd build
    
5. Configure and build!

    cmake .. && make