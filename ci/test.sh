#!/bin/bash
set -e

phdb_version=$1
phdb_buildno=$2
distro=$3
arch=amd64

pkg_version=$phdb_version-$phdb_buildno~$distro
serverpkg=phoenixdb=$pkg_version
utilspkg=phoenixdb-utils=$pkg_version
libpkg=libphoenixdb=$pkg_version
pluginspkg=phoenixdb-plugins=$pkg_version
src=$pkg.tar.gz
results=`pwd`/results.xml

echo deb http://packages.phoenixdb.org/deb $distro qa > /etc/apt/sources.list.d/phoenixdb.list

apt-get update

apt-get install -y --force-yes $serverpkg $utilspkg $libpkg $pluginspkg

service postgresql start

su -l postgres -c $'psql -c "CREATE USER phoenixdb WITH PASSWORD \'phoenixdb\';"'
su -l postgres -c $'psql -c "CREATE DATABASE phoenixdb OWNER phoenixdb;"'

cp /etc/phoenixdb/phoenixdb-sample.conf /etc/phoenixdb/phoenixdb.conf

sed -e s,RUN_PHOENIXDB=no,RUN_PHOENIXDB=yes, -e s,INSTANCES_COUNT=.\*,INSTANCES_COUNT=2,  -i /etc/default/phoenixdb

echo yes | /etc/init.d/phoenixdb init
echo yes | /etc/init.d/phoenixdb register

/etc/init.d/phoenixdb start

sleep 5

cd tests

cat test.env.in | sed -e s,@CONFIGURE_PREFIX@,/usr, \
    -e s,@CONFIGURE_DATA_DIR@,`pwd`/harness/testcases/data, \
    -e s,@CONFIGURE_UTILS_DIR@,`pwd`/utils, \
    -e s,@CONFIGURE_PATH@,, \
    -e s,@CONFIGURE_TESTSROOTDIR@,`pwd`/harness/testcases/t, > test.env

phoenixqa/phoenixqa.py --results=$results -E ./test.env -e harness/testcases/*.exclude

cat $results | tr -dc '[\012\040-\176]' > tmp
mv tmp $results
