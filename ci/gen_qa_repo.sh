#!/bin/bash
set -e
repo_root=/opt/repository/deb
distro=$distro
section=qa
repo_dist=dists/$distro/$section
repo_pool=pool/$distro/$section

rm -rf $repo_root/$repo_dist $repo_root/$repo_pool

mkdir -p $repo_root/$repo_dist/{binary-amd64,binary-i386,source} $repo_root/$repo_pool

cp *.deb *.dsc *.tar.gz $repo_root/$repo_pool

cd $repo_root

dpkg-scanpackages -a amd64 $repo_pool > $repo_dist/binary-amd64/Packages
dpkg-scanpackages -a i386 $repo_pool > $repo_dist/binary-i386/Packages

echo "Archive: $distro" > dists/$distro/Release
echo "Suite: $distro" >> dists/$distro/Release
echo "Components: qa" >> dists/$distro/Release
echo "Origin: phoenixdb.org" >> dists/$distro/Release
echo "Label: PhoenixDB $distro repository" >> dists/$distro/Release
echo "Architectures: amd64" >> dists/$distro/Release
echo "Description: PhoenixDB $distro repository" >> dists/$distro/Release
apt-ftparchive release dists/$distro >> dists/$distro/Release

