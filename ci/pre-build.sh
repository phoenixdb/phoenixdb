#!/bin/bash
set -e

PHDB_VERSION=`cat version`
PHDB_REVISION=`git rev-parse HEAD`
PHDB_REVISION_SHORT=`echo $PHDB_REVISION|cut -c-8`

if [ "$CUSTOM_BUILDNO" = "" ]; then
    case $BUILD_TYPE in
    	snapshot)
        	PHDB_BUILDNO=snapshot-`date +%Y%m%d%H%M`-$PHDB_REVISION_SHORT
        ;;
        rc)
            PHDB_BUILDNO=rc-0-$PHDB_REVISION_SHORT
        ;;
        release)
            PHDB_BUILDNO=release-0
        ;;
        *)
            echo Unknown build type $BUILD_TYPE
            exit 1
        ;;
    esac
else
    PHDB_BUILDNO=$CUSTOM_BUILDNO
fi

f=env.properties
echo -n > $f
echo PHDB_VERSION=$PHDB_VERSION >> $f
echo PHDB_REVISION=$PHDB_REVISION >> $f
echo PHDB_REVISION_SHORT=$PHDB_REVISION_SHORT >> $f
echo PHDB_BUILDNO=$PHDB_BUILDNO >> $f
echo PHDB_BUILD_TYPE=$BUILD_TYPE >> $f

echo env.properties
cat env.properties
