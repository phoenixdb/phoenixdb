########################################
# BEGIN_COPYRIGHT
#
# Copyright (C) 2008-2015 SciDB, Inc.
# All Rights Reserved.
#
# SciDB is free software: you can redistribute it and/or modify
# it under the terms of the AFFERO GNU General Public License as published by
# the Free Software Foundation.
#
# SciDB is distributed "AS-IS" AND WITHOUT ANY WARRANTY OF ANY KIND,
# INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
# NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE. See
# the AFFERO GNU General Public License for the complete license terms.
#
# You should have received a copy of the AFFERO GNU General Public License
# along with SciDB.  If not, see <http://www.gnu.org/licenses/agpl-3.0.html>
#
# END_COPYRIGHT
########################################

# C O M P O N E N T S
#phoenixdb package
if (NOT WITHOUT_SERVER)
    install(PROGRAMS "${OUT_BIN}/phoenixdb" DESTINATION ${BIN} COMPONENT phoenixdb)
    install(PROGRAMS ${OUT_ETC}/init.d/phoenixdb DESTINATION ${ETC}/init.d COMPONENT phoenixdb)

    install(FILES ${OUT_SHARE}/prelude.txt DESTINATION ${SHARE} COMPONENT phoenixdb)

    install(FILES ${OUT_ETCPHOENIXDB}/log4cxx.properties.install DESTINATION ${ETCPHOENIXDB} RENAME log4cxx.properties COMPONENT phoenixdb)
    install(FILES ${OUT_ETCPHOENIXDB}/phoenixdb-sample.conf DESTINATION ${ETCPHOENIXDB} COMPONENT phoenixdb)
    install(FILES ${OUT_ETCDEFAULT}/phoenixdb.install DESTINATION ${ETCDEFAULT} RENAME phoenixdb COMPONENT phoenixdb)
endif()

#phoenixdb-client package
install(FILES "${OUT_LIB}/libphoenixdbclient${CMAKE_SHARED_LIBRARY_SUFFIX}" DESTINATION ${LIB} COMPONENT phoenixdb-client)

#phoenixdb-utils package
install(PROGRAMS "${OUT_BIN}/iaql" DESTINATION ${BIN} COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/gen_matrix" DESTINATION ${MISCBIN} COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/loadpipe.py" DESTINATION ${MISCBIN} COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/calculate_chunk_length.py" DESTINATION ${MISCBIN} COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/scidblib/PSF_license.txt" DESTINATION ${MISCBIN}/scidblib COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/scidblib/__init__.py" DESTINATION ${MISCBIN}/scidblib COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/scidblib/scidb_math.py" DESTINATION ${MISCBIN}/scidblib COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/scidblib/scidb_progress.py" DESTINATION ${MISCBIN}/scidblib COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/scidblib/scidb_schema.py" DESTINATION ${MISCBIN}/scidblib COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/scidblib/scidb_afl.py" DESTINATION ${MISCBIN}/scidblib COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/scidblib/statistics.py" DESTINATION ${MISCBIN}/scidblib COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/scidblib/util.py" DESTINATION ${MISCBIN}/scidblib COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/scidblib/counter.py" DESTINATION ${MISCBIN}/scidblib COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/scidblib/scidb_psf.py" DESTINATION ${MISCBIN}/scidblib COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/scidblib/scidb_control.py" DESTINATION ${MISCBIN}/scidblib COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/scidblib/pgpass_updater.py" DESTINATION ${MISCBIN}/scidblib COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/scidb_backup.py" DESTINATION ${MISCBIN} COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/scidb_cores" DESTINATION ${MISCBIN} COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/remove_arrays.py" DESTINATION ${MISCBIN} COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/bellman_ford_example.sh" DESTINATION ${MISCBIN} COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/pagerank_example.sh" DESTINATION ${MISCBIN} COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/doSvd.sh" DESTINATION ${MISCBIN} COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/doSvdDirect.sh" DESTINATION ${MISCBIN} COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/doSvdWithMetrics.sh" DESTINATION ${MISCBIN} COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/doSvdMetric.sh" DESTINATION ${MISCBIN} COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/doSvdWithMetrics.sh" DESTINATION ${MISCBIN} COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/diag.sh" DESTINATION ${MISCBIN} COMPONENT phoenixdb-utils)
install(PROGRAMS "${OUT_MISCBIN}/dla_driver.py" DESTINATION ${MISCBIN} COMPONENT phoenixdb-utils)


#phoenixdb-dev-tools package
if(CPPUNIT_FOUND)
	install(PROGRAMS "${CMAKE_BINARY_DIR}/tests/unit/unit_tests" DESTINATION bin COMPONENT phoenixdb-dev-tools)
endif()

#
# P L U G I N S
#

#phoenixdb-plugins package
install(FILES "${OUT_PLUGINS}/libpoint${CMAKE_SHARED_LIBRARY_SUFFIX}" DESTINATION ${PLUGINS} COMPONENT phoenixdb-plugins)
install(FILES "${OUT_PLUGINS}/libmatch${CMAKE_SHARED_LIBRARY_SUFFIX}" DESTINATION ${PLUGINS} COMPONENT phoenixdb-plugins)
install(FILES "${OUT_PLUGINS}/libbestmatch${CMAKE_SHARED_LIBRARY_SUFFIX}" DESTINATION ${PLUGINS} COMPONENT phoenixdb-plugins)
install(FILES "${OUT_PLUGINS}/librational${CMAKE_SHARED_LIBRARY_SUFFIX}" DESTINATION ${PLUGINS} COMPONENT phoenixdb-plugins)
install(FILES "${OUT_PLUGINS}/libcomplex${CMAKE_SHARED_LIBRARY_SUFFIX}" DESTINATION ${PLUGINS} COMPONENT phoenixdb-plugins)
install(FILES "${OUT_PLUGINS}/libra_decl${CMAKE_SHARED_LIBRARY_SUFFIX}" DESTINATION ${PLUGINS} COMPONENT phoenixdb-plugins)
install(FILES "${OUT_PLUGINS}/libmore_math${CMAKE_SHARED_LIBRARY_SUFFIX}" DESTINATION ${PLUGINS} COMPONENT phoenixdb-plugins)
install(FILES "${OUT_PLUGINS}/libmisc${CMAKE_SHARED_LIBRARY_SUFFIX}" DESTINATION ${PLUGINS} COMPONENT phoenixdb-plugins)
install(FILES "${OUT_PLUGINS}/libprototype_load_tools${CMAKE_SHARED_LIBRARY_SUFFIX}" DESTINATION ${PLUGINS} COMPONENT phoenixdb-plugins)
install(FILES "${OUT_PLUGINS}/libtile_integration${CMAKE_SHARED_LIBRARY_SUFFIX}" DESTINATION ${PLUGINS} COMPONENT phoenixdb-plugins)
install(FILES "${OUT_PLUGINS}/libfits${CMAKE_SHARED_LIBRARY_SUFFIX}" DESTINATION ${PLUGINS} COMPONENT phoenixdb-plugins)
install(FILES "${OUT_PLUGINS}/libdense_linear_algebra${CMAKE_SHARED_LIBRARY_SUFFIX}" DESTINATION ${PLUGINS} COMPONENT phoenixdb-plugins)
install(FILES "${OUT_PLUGINS}/liblinear_algebra${CMAKE_SHARED_LIBRARY_SUFFIX}" DESTINATION ${PLUGINS} COMPONENT phoenixdb-plugins)
install(FILES "${OUT_PLUGINS}/libexample_udos${CMAKE_SHARED_LIBRARY_SUFFIX}" DESTINATION ${PLUGINS} COMPONENT phoenixdb-plugins)

install(FILES "${OUT_MISCLIB}/libphdbmpi${CMAKE_SHARED_LIBRARY_SUFFIX}" DESTINATION ${MISCLIB} COMPONENT phoenixdb-plugins)

if (TARGET mpi_slave_scidb)
    install(PROGRAMS "${OUT_MISCBIN}/mpi_slave_scidb" DESTINATION ${MISCBIN} COMPONENT phoenixdb-plugins)
endif ()

#phoenixdb-dev package
install(DIRECTORY ${CMAKE_SOURCE_DIR}/include/ DESTINATION ${INCLUDE} COMPONENT phoenixdb-dev PATTERN ".svn" EXCLUDE)

