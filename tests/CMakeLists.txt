########################################
# BEGIN_COPYRIGHT
#
# Copyright (C) 2008-2015 SciDB, Inc.
# All Rights Reserved.
#
# SciDB is free software: you can redistribute it and/or modify
# it under the terms of the AFFERO GNU General Public License as published by
# the Free Software Foundation.
#
# SciDB is distributed "AS-IS" AND WITHOUT ANY WARRANTY OF ANY KIND,
# INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
# NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE. See
# the AFFERO GNU General Public License for the complete license terms.
#
# You should have received a copy of the AFFERO GNU General Public License
# along with SciDB.  If not, see <http://www.gnu.org/licenses/agpl-3.0.html>
#
# END_COPYRIGHT
########################################

add_subdirectory("unit")
add_subdirectory("benchmarks")

#
# Configure test environment for developer mode
#
if(DEVELOPER)
	set(CONFIGURE_DATA_DIR "${CMAKE_CURRENT_SOURCE_DIR}/harness/testcases/data")
	set(CONFIGURE_UTILS_DIR "${CMAKE_CURRENT_SOURCE_DIR}/utils")
	set(CONFIGURE_PATH "${CMAKE_CURRENT_BINARY_DIR}/unit")
	set(CONFIGURE_PREFIX "${OUT_COMMON}")
	set(CONFIGURE_TESTSROOTDIR "${CMAKE_CURRENT_SOURCE_DIR}/harness/testcases/t")
	configure_file(${CMAKE_CURRENT_SOURCE_DIR}/test.env.in ${CMAKE_CURRENT_SOURCE_DIR}/phoenixqa/test.env @ONLY)
endif()
