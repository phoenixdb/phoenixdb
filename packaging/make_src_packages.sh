#!/bin/bash
#
# BEGIN_COPYRIGHT
#
# Copyright (C) 2008-2015 SciDB, Inc.
# All Rights Reserved.
#
# SciDB is free software: you can redistribute it and/or modify
# it under the terms of the AFFERO GNU General Public License as published by
# the Free Software Foundation.
#
# SciDB is distributed "AS-IS" AND WITHOUT ANY WARRANTY OF ANY KIND,
# INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
# NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE. See
# the AFFERO GNU General Public License for the complete license terms.
#
# You should have received a copy of the AFFERO GNU General Public License
# along with SciDB.  If not, see <http://www.gnu.org/licenses/agpl-3.0.html>
#
# END_COPYRIGHT
#

set -e

function die()
{
  echo $*
  exit 1
}

function make_src()
{
    modules="extern/libcsv extern/cityhash tests/phoenixqa"
    cd $SRC_DIR
    src_temp=`mktemp -d`
    echo "Building source package in $src_temp"
    
    echo "Extracting phoenixdb sources"
    hash=`git stash create`
    [ "$hash" = "" ] && hash=HEAD
    git archive $hash | tar -xC "${src_temp}"

    echo "Extracting submodule sources"
    for m in $modules; do
        echo "  $m"
        cd $SRC_DIR/$m
        mkdir -p ${src_temp}/$m
        git archive HEAD | tar -xC "${src_temp}/$m"
    done

    echo "Adding revision file to sources"
    cd $SRC_DIR
    echo $REVISION > ${src_temp}/revision

    echo "Archiving"
    cd $src_temp
    tar --transform "s|^.|$SRC_PKG_NAME|" -czf $SRC_DIR/$SRC_PKG_NAME.tar.gz .
    cd $SRC_DIR
    rm -rf "$src_temp"
}

function make_deb_src()
{
    distribution=$1
    echo "Building source packages for distro $distribution"

    cd $SRC_DIR
    rm -rf $SRC_PKG_NAME
    tar xf $SRC_PKG_NAME.tar.gz
    cd $SRC_PKG_NAME
    rm -rf tests

    M4="m4 -DVERSION_MAJOR=${VERSION_MAJOR} -DVERSION_MINOR=${VERSION_MINOR} -DVERSION_PATCH=${VERSION_PATCH} -DBUILDNO=${BUILDNO} -DCODENAME=$distribution"

    mkdir -p debian/source
    cp packaging/deb/source/format debian/source
    cp packaging/deb/{compat,*.install,postinst,rules,control} debian   
    $M4 packaging/deb/changelog.in > debian/changelog

    if [ "$SIGN_PACKAGES" = 1 ]; then
        dpkg-buildpackage -rfakeroot -S
    else
        dpkg-buildpackage -rfakeroot -S -uc -us
    fi
    rm -rf "$SRC_PKG_NAME"
}

SRC_DIR=$(readlink -f $(dirname $0)/..)

echo Sources dir is $SRC_DIR

cd ${SRC_DIR}
echo Extracting version
VERSION_MAJOR=`awk -F . '{print $1}' version`
VERSION_MINOR=`awk -F . '{print $2}' version`
VERSION_PATCH=`awk -F . '{print $3}' version`
VERSION_BUILDNO=${BUILDNO:=SNAPSHOT-`date +%Y%m%d%H%M`}

echo "Extracting revision from git"
REVISION=$(git rev-parse HEAD)

echo "Version is $VERSION_MAJOR.$VERSION_MINOR.$VERSION_PATCH"
echo "Build number is $VERSION_BUILDNO"

PKGNAME=phoenixdb
SRC_PKG_NAME=$PKGNAME-$VERSION_MAJOR.$VERSION_MINOR.$VERSION_PATCH

make_src
make_deb_src precise
make_deb_src trusty
make_deb_src vivid
make_deb_src wily
