+--upgrade from 3 to 4
+
+drop function uuid_generate_v1();
+
+update "cluster" set metadata_version = 4;
