/*
**
* BEGIN_COPYRIGHT
*
* Copyright (C) 2008-2015 SciDB, Inc.
* All Rights Reserved.
*
* SciDB is free software: you can redistribute it and/or modify
* it under the terms of the AFFERO GNU General Public License as published by
* the Free Software Foundation.
*
* SciDB is distributed "AS-IS" AND WITHOUT ANY WARRANTY OF ANY KIND,
* INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
* NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE. See
* the AFFERO GNU General Public License for the complete license terms.
*
* You should have received a copy of the AFFERO GNU General Public License
* along with SciDB.  If not, see <http://www.gnu.org/licenses/agpl-3.0.html>
*
* END_COPYRIGHT
*/

/**
 * @file ThreadPool.cpp
 *
 * @author roman.simakov@gmail.com
 *
 * @brief The ThreadPool class
 */

#include <util/ThreadPool.h>

#include <log4cxx/logger.h>

namespace scidb
{

static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("scidb.common.thread"));

ThreadPool::ThreadPool(size_t threadCount, std::shared_ptr<JobQueue> queue)
: _queue(queue),
  _threadCount(threadCount)
{
    _shutdown = false;
    if (_threadCount <= 0) {
        throw InvalidArgumentException(REL_FILE, __FUNCTION__, __LINE__)
        << "thread count";
    }
}

void ThreadPool::start()
{
    ScopedMutexLock lock(_mutex);

    if (_shutdown) {
        throw AlreadyStoppedException(REL_FILE, __FUNCTION__, __LINE__)
        << "thread pool cannot be started after being stopped";
    }
    if (_threads.size() > 0) {
        throw AlreadyStartedException(REL_FILE, __FUNCTION__, __LINE__)
        << "thread pool can be started only once";
    }

    for (size_t i = 0; i < _threadCount; i++)
    {
        _threads.create_thread(boost::ref(*this));
        getInjectedErrorListener().check();
    }
}

bool ThreadPool::isStarted()
{
    ScopedMutexLock lock(_mutex);
    return _threads.size() > 0;
}

void ThreadPool::stop()
{
    { // scope
        ScopedMutexLock lock(_mutex);
        if (_shutdown) {
            return;
        }
        _shutdown = true;
    }

    _threads.interrupt_all();
    _threads.join_all();
}

void ThreadPool::operator()()
{
    LOG4CXX_TRACE(logger, "Thread::threadFunction: begin tid = "
                  << psync::ThisThread::get_id()
                  << ", pool = " << this);

    while (true)
    {
        try
        {
            std::shared_ptr<Job> job = getQueue()->popJob();
            {
                ScopedMutexLock lock(_mutex);
                if (_shutdown) {
                    break;
                }
            }
            job->execute();
        }
        // Need to check error type. If fatal stops execution. If no to continue.
        catch (const std::exception& e)
        {
            try {  // This try catch block must prevent crash if there is no space on disk where log file is located.
                LOG4CXX_ERROR(logger, "Thread::threadFunction: unhandled exception: "
                              << e.what()
                              << ", tid = " << psync::ThisThread::get_id()
                              << ", pool = " << this);
            } catch (...) {}
            throw;
        }
        catch (SCIDB_THREAD_INTERRUPTION &)
        {
            if (_shutdown) break;
            else LOG4CXX_ERROR(logger, "job was aborted");
        }
        catch (...)
        {
            LOG4CXX_ERROR(logger, "unhandled exception in job");
        }
    }

    LOG4CXX_TRACE(logger, "Thread::threadFunction: end tid = "
                  << psync::ThisThread::get_id()
                  << ", pool = " << this);
}

InjectedErrorListener<ThreadStartInjectedError> ThreadPool::s_injectedErrorListener;

} //namespace
