/*
**
* BEGIN_COPYRIGHT
*
* Copyright (C) 2008-2015 SciDB, Inc.
* All Rights Reserved.
*
* SciDB is free software: you can redistribute it and/or modify
* it under the terms of the AFFERO GNU General Public License as published by
* the Free Software Foundation.
*
* SciDB is distributed "AS-IS" AND WITHOUT ANY WARRANTY OF ANY KIND,
* INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
* NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE. See
* the AFFERO GNU General Public License for the complete license terms.
*
* You should have received a copy of the AFFERO GNU General Public License
* along with SciDB.  If not, see <http://www.gnu.org/licenses/agpl-3.0.html>
*
* END_COPYRIGHT
*/

/**
 * @file Semaphore.cpp
 *
 * @author roman.simakov@gmail.com
 *
 * @brief The Semaphore class
 */

#include "assert.h"
#include <stdio.h>
#include <string>
#include <sstream>
#include <sys/time.h>
#include <system/Exceptions.h>
#include <util/Semaphore.h>

namespace scidb
{

Semaphore::Semaphore(): _count(0)
{
}

Semaphore::~Semaphore()
{ 
}

Semaphore::Semaphore(const Semaphore&)
    : _count(0)
{
}

Semaphore& Semaphore::operator=(const Semaphore&)
{
    return *this;
}

bool Semaphore::enter(int n, ErrorChecker& errorChecker, int timeout)
{
    ScopedMutexLock mutexLock(_cs);
    while (_count < n) {
        // take what is possible currently to prevent infinity waiting
        n -= _count;
        _count = 0;
        // wait for new releases
        if (!_cond.wait(_cs, errorChecker, timeout)) {
            return false;
        }
    }
    _count -= n;
    return true;
}

void Semaphore::enter(int n)
{
    ScopedMutexLock mutexLock(_cs);
    while (_count < n) {
        ErrorChecker errorChecker;
        // take what is possible currently to prevent infinity waiting
        n -= _count;
        _count = 0;
        // wait for new releases
        _cond.wait(_cs, errorChecker);
    }
    _count -= n;
}

void Semaphore::release(int n)
{
    ScopedMutexLock mutexLock(_cs);
    _count += n;
    _cond.signal();
}

bool Semaphore::tryEnter()
{
    ScopedMutexLock mutexLock(_cs);
    if (_count > 0) {
        _count--;
        return true;
    }
    return false;
}

} //namespace
