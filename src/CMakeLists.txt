########################################
# BEGIN_COPYRIGHT
#
# Copyright (C) 2008-2015 SciDB, Inc.
# All Rights Reserved.
#
# SciDB is free software: you can redistribute it and/or modify
# it under the terms of the AFFERO GNU General Public License as published by
# the Free Software Foundation.
#
# SciDB is distributed "AS-IS" AND WITHOUT ANY WARRANTY OF ANY KIND,
# INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
# NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE. See
# the AFFERO GNU General Public License for the complete license terms.
#
# You should have received a copy of the AFFERO GNU General Public License
# along with SciDB.  If not, see <http://www.gnu.org/licenses/agpl-3.0.html>
#
# END_COPYRIGHT
########################################

add_subdirectory("capi")
add_subdirectory("network")
add_subdirectory("system")
add_subdirectory("smgr")
add_subdirectory("usr_namespace")
add_subdirectory("util")
add_subdirectory("mpi")
add_subdirectory("dense_linear_algebra")
add_subdirectory("linear_algebra")

if (NOT WITHOUT_SERVER)
    add_subdirectory("array")
    add_subdirectory("query")

    # $include_src is not necessary. It is a convenience for the qtcreator and xcode IDEs
    file(GLOB include_src "../include/*.h" "../include/array/*.h" "../include/system/*.h"
            "../include/query/*.h" "../include/util/*.h")

    set(phdb_src network/entry.cpp network/GlobalNew.cpp)    
    if (BLAS_LIBRARIES AND LAPACK_LIBRARIES)
        set(phdb_src ${phdb_src} dense_linear_algebra/blas/initMathLibs.cpp)
    endif()

    add_executable(phoenixdb ${phdb_src} ${include_src})

    # removes the symbols from phoenixdb and puts them in a separate file
    set_target_properties(phoenixdb PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${OUT_BIN})
    add_dependencies(phoenixdb cityhash)
    target_link_libraries(phoenixdb network_lib util_lib system_lib qproc_lib array_lib
        ${LIBRT_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT} ${CMAKE_DL_LIBS} ${CITYHASH_LIBRARY})
    
    if (BLAS_LIBRARIES AND LAPACK_LIBRARIES)
        target_link_libraries(phoenixdb ${BLAS_LIBRARIES} ${LAPACK_LIBRARIES})
    endif()
endif()
