/*
**
* BEGIN_COPYRIGHT
*
* Copyright (C) 2008-2015 SciDB, Inc.
* All Rights Reserved.
*
* SciDB is free software: you can redistribute it and/or modify
* it under the terms of the AFFERO GNU General Public License as published by
* the Free Software Foundation.
*
* SciDB is distributed "AS-IS" AND WITHOUT ANY WARRANTY OF ANY KIND,
* INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
* NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE. See
* the AFFERO GNU General Public License for the complete license terms.
*
* You should have received a copy of the AFFERO GNU General Public License
* along with SciDB.  If not, see <http://www.gnu.org/licenses/agpl-3.0.html>
*
* END_COPYRIGHT
*/

/**
 * @file Mutex.h
 *
 * @author roman.simakov@gmail.com
 *
 * @brief The Mutex class for synchronization
 */

#ifndef MUTEX_H_
#define MUTEX_H_

#include <assert.h>
#include <stdlib.h>
#include <system/Exceptions.h>
#include <util/Sync.h>

namespace scidb
{

class Mutex
{
friend class Event;
friend class Semaphore;
private:
    psync::Mutex _mutex;
  public:
    Mutex(){}
    ~Mutex(){}

    Mutex(const Mutex&){}
    Mutex& operator=(const Mutex&)
    {
        return *this;
    }

    void lock()
    {
        _mutex.lock();
    }
    void unlock()
    {
        _mutex.unlock();
    }
    void checkForDeadlock(){}
};


/***
 * RAII class for holding Mutex in object visible scope.
 */
class ScopedMutexLock
{
private:
	Mutex& _mutex;

public:
	ScopedMutexLock(Mutex& mutex): _mutex(mutex)
	{
		_mutex.lock();
	}

	~ScopedMutexLock()
	{
		_mutex.unlock();
	}
};


} //namespace

#endif /* MUTEX_H_ */
