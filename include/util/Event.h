/*
**
* BEGIN_COPYRIGHT
*
* Copyright (C) 2008-2015 SciDB, Inc.
* All Rights Reserved.
*
* SciDB is free software: you can redistribute it and/or modify
* it under the terms of the AFFERO GNU General Public License as published by
* the Free Software Foundation.
*
* SciDB is distributed "AS-IS" AND WITHOUT ANY WARRANTY OF ANY KIND,
* INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
* NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE. See
* the AFFERO GNU General Public License for the complete license terms.
*
* You should have received a copy of the AFFERO GNU General Public License
* along with SciDB.  If not, see <http://www.gnu.org/licenses/agpl-3.0.html>
*
* END_COPYRIGHT
*/

/**
 * @file Event.h
 *
 * @author knizhnik@garret.ru, roman.simakov@gmail.com
 *
 * @brief POSIX conditional variable
 */

#ifndef EVENT_H_
#define EVENT_H_

#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <errno.h>
#include <boost/function.hpp>
#include <util/Mutex.h>
#include <system/Exceptions.h>

namespace scidb
{

class Event
{
private:
    psync::Event  _cond;
    bool signaled;

public:
    /**
     * @throws a scidb::Exception if necessary
     */
    typedef boost::function<bool()> ErrorChecker;

    Event()
    {
        signaled = false;
    }

    ~Event()
    {
    }

    /**
     * Wait for the Event to become signalled (based on the POSIX (timed) conditional variable+mutex).
     * If the event is signalled before the call to wait,
     * the wait will never return. The signal will cause the wait()'s of *all* threads to return.
     * @param cs associated with this event, the same mutex has to be used the corresponding wait/signal
     * @param errorChecker if set, it will be invoked periodically and
     *                     the false return code will force this wait() to return regardless of whether the signal is received.
     * @param timeout if not 0 set timeout of waiting in microseconds.
     * @note The errorChecker must also check for the condition predicate for which this Event is used because of the unavoidable
     *       race condition between the timer expiring (in pthread_cond_timedwait) and another thread signalling.
     */

    bool wait(Mutex& cs, ErrorChecker& errorChecker, int timeout = 0)
    {
        cs.checkForDeadlock();
        if (errorChecker && !timeout) {
            if (!errorChecker()) {
               return false;
            }

            signaled = false;
            do {
                boost::xtime t;
                t.sec = 10;
                if (_cond.timed_wait(cs._mutex, t)) {
                    return true;
                }
                if (!errorChecker()) {
                   return false;
                }
            } while (!signaled);
        }
        else
        {
            if (!timeout)
                _cond.wait(cs._mutex);
            else
            {
                boost::xtime t;
                t.nsec = timeout * 1000;
                return _cond.timed_wait(cs._mutex, t);
            }
        }
        return true;
    }

    void signal()
    {
        signaled = true;
        _cond.notify_all();
    }
};

}

#endif
