/*
**
* BEGIN_COPYRIGHT
*
* This file is part of OpenSciDB.
* Copyright (C) 2013 OpenSciDB Team
* Copyright (C) 2008-2013 SciDB, Inc.
*
* OpenSciDB is free software: you can redistribute it and/or modify
* it under the terms of the AFFERO GNU General Public License as published by
* the Free Software Foundation.
*
* OpenSciDB is distributed "AS-IS" AND WITHOUT ANY WARRANTY OF ANY KIND,
* INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
* NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE. See
* the AFFERO GNU General Public License for the complete license terms.
*
* You should have received a copy of the AFFERO GNU General Public License
* along with OpenSciDB.  If not, see <http://www.gnu.org/licenses/agpl-3.0.html>
*
* END_COPYRIGHT
*/

#ifndef _SYNC_H_
#define _SYNC_H_

#include <boost/thread.hpp>

namespace scidb
{
namespace psync
{

typedef boost::condition_variable_any Event;
typedef boost::recursive_mutex Mutex;
typedef boost::thread_group ThreadGroup;
namespace ThisThread = boost::this_thread;
#define SCIDB_THREAD_INTERRUPTION boost::thread_interrupted

} //namespace sync
} //namespace scidb

#endif /* _SYNC_H_ */
