/*
**
* BEGIN_COPYRIGHT
*
* This file is part of PhoenixDB.
* Copyright (C) 2013 PhoenixDB Team
* Copyright (C) 2008-2013 SciDB, Inc.
*
* PhoenixDB is free software: you can redistribute it and/or modify
* it under the terms of the AFFERO GNU General Public License as published by
* the Free Software Foundation.
*
* PhoenixDB is distributed "AS-IS" AND WITHOUT ANY WARRANTY OF ANY KIND,
* INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
* NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE. See
* the AFFERO GNU General Public License for the complete license terms.
*
* You should have received a copy of the AFFERO GNU General Public License
* along with PhoenixDB.  If not, see <http://www.gnu.org/licenses/agpl-3.0.html>
*
* END_COPYRIGHT
*/

/**
 * @file Semaphore.h
 *
 * @author roman.simakov@gmail.com
 *
 * @brief The Semaphore class for synchronization
 */

#ifndef SEMAPHORE_H_
#define SEMAPHORE_H_

#include <errno.h>
#include <boost/function.hpp>
#include <string>

#include <util/Mutex.h>
#include <util/Event.h>

namespace scidb
{

class Semaphore
{
private:
    Event _cond;
    Mutex _cs;
    int _count;

public:
     /**
      * @throws a scidb::Exception if necessary
      */
    typedef boost::function<bool()> ErrorChecker;
    Semaphore();
    ~Semaphore();

    Semaphore(const Semaphore&);
    Semaphore& operator=(const Semaphore&);

    bool enter(ErrorChecker& errorChecker) {
       return enter(1, errorChecker);
    }

    void enter(int count = 1);
    /**
     * @param timeout if not 0 set timeout of waiting in microseconds.
     */
    bool enter(int count, ErrorChecker& errorChecker, int timeout = 0);

    void release(int n = 1);
    bool tryEnter();
};

class ReleaseOnExit
{
    Semaphore& sem;

  public:
    ReleaseOnExit(Semaphore& s) : sem(s) {}

    ~ReleaseOnExit() {
        sem.release();
    }
};

} //namespace


#endif /* SEMAPHORE_H_ */
